'use strict'

module.exports = {
  auth: {
    "databases": {
      "allstar": {
        "host": "0.0.0.0"
      , "db": "allstar_auth"
      , "default": true
      , "driver": "rethinkdb"
      , "port": 28015
      }
    }
  }
, nats: {
    user: null
  , pass: null
  , servers: [
      "nats://0.0.0.0:4222"
    , "nats://0.0.0.0:4223"
    , "nats://0.0.0.0:4224"
    ]
  }
, jaeger: {
    serviceName: 'auth'
  , delegateTags: [{
      key: 'op'
    , tag: 'allstar.db.query'
    }]
  , jaeger: {
      sampler: {
        type: 'Const',
        options: true
      },
      options: {
        tags: {
          'hemera.version':`Node-${require('nats-hemera/package.json').version}`
        , 'nodejs.version': process.versions.node
        }
      },
      reporter: {
        host: 'localhost'
      }
    }
  }
}
