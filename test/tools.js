'use strict'

const {promisify} = require('util')
const Hemera = require('nats-hemera')
const nats = require('nats')
const conf = require('keef')
const log = require('bitters')
const sloader = require('../lib/fs/loading/service')
const mloader = require('../lib/fs/loading/model')

const parse = require('@allstar/parse-hosts')
const acl = require('@allstar/hemera-acl')

const nats_config = conf.get('nats')

const connection = {
  ...nats_config
, servers: parse(nats_config.servers)
}

log.debug('nats config', connection)


if (require.main === module) return

let hemera
let db

module.exports = {
  setup
, teardown
}

async function setup() {
  db = require('zim/lib/db').connection

  const nc = nats.connect(connection)
  const services = sloader.load().flat()
  const models = mloader.load().flat()

  const r = db.default.r
  const cached = Object.entries(db.default.models)
  hemera = new Hemera(nc, {logLevel: 'trace', tag: 'auth-test' })
  hemera.use(acl)

  await new Promise((resolve) => {
    hemera.ready(resolve)
  })

  log.info('hemera loaded')
  await db.default.dbReady()
  await r.db('test').wait()

  for (const [name, model] of cached) {
    await model.indexes()
    log.info(`table ${name} indexes ready`)
  }

  await new Promise((resolve) => {
    setTimeout(resolve, 1000)
  })

  log.info('db ready')
  return {hemera, db, services, models}
}

async function teardown() {

  await new Promise((resolve, reject) => {

    hemera = require('../lib/hemera')
    if (!hemera) return resolve()
    log.info('shuttind down hemera')
    hemera.close(resolve)
  })

  if (!db) return

  log.info('shutting down db connections')
  const r = db.default.r
  await r.db('test').wait()
  const models = Object.entries(db.default.models)

  for (const [name, model] of models) {
    log.info(`dropping ${name}`)
    await r.table(name).wait()
    await r.tableDrop(name)
  }
  await r.getPool().drain()
  log.info('db disconnected')
  return null
}

async function tableReady(r, name) {
  var waiting = true
  while(waiting){
    try {
      await r.table(name).wait()
      waiting = false
      break
    } catch(_) {}
  }
  return null
}
