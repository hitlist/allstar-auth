'use strict'

if (require.main === module) return

const util = require('util')
const tap = require('tap')
const services = require('../lib/fs/loading/service')
let db
let hemera
module.exports = {
  setup
, teardown
}

function setup () {
  return new Promise((resolve, reject) => {
    db = require('zim/lib/db')
    hemera = require('../lib/hemera')
    hemera.ready(() => {
      services.load()
      const {r, models} = db.connection.default
      const model = Object.keys(models)[0]
      const table = models[model].getTableName()
      r.table(table).wait().then(() => {
        resolve({
          r: r
        , models: models
        , hemera: hemera
        })
      }).catch(reject)
    })
  })
}

async function teardown() {
  const {r, models} = db.connection.default

  for (const model of Object.values(models)) {
    console.log(`deleting ${model.getTableName()}`)
    await r.tableDrop(model.getTableName())
  }
  if (db) db.connection.default.r.getPool().drain()
  if (hemera) util.promisify(hemera.close.bind(hemera))()
  setImmediate(process.exit, 0)
}
