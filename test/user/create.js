'use strict'
const uuid = require('uuid')
const {test} = require('tap')
const {setup, teardown} = require('../init')

test('user::create', async (t) => {
  const {r, models, hemera} = await setup()

  await t.test('setup', async (tt) => {
    tt.ok(r, 'r connection')
    tt.ok(models, 'Models cache')
    tt.ok(hemera, 'hemera')
  }).catch(t.threw)

  await t.test('action - create yields a new user', async (tt) => {
    const user = await hemera.act({
      topic: 'user'
    , cmd: 'create'
    , version: 'v1'
    , meta$: {
        user: {
          id: uuid.v4()
        , content_type: 'system'
        , permissions: {
            auth: {user: { create: true }}
          }
        }
      }
    , username: 'joeblow'
    , email: 'joeblow@fakemail.gov'
    , name: {
        first: 'joe'
      , last: 'blow'
      }
    })

    tt.ok(user.allstar_auth_user_id, 'id')
    tt.end()
  }).catch(t.threw)
  t.end()
})

test('teardown', async (t) => {
  await teardown()
  t.end()
})
