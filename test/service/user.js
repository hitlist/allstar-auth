'use strict'

const {test, threw, pass} = require('tap')
const {setup, teardown} = require('../tools')
const fixtures = {
  role: require('../fixture/role')
}

test('services::user', async (t) => {
  const {db, hemera, models} = await setup()
  const roles = await new models.Role(fixtures.role[0]).save()
  t.pass('setup complete')

  await t.test('create', async (tt) => {
    const user = await hemera.act({
      topic: 'user'
    , cmd: 'create'
    , version: 'v1'
    , username: 'userone'
    , email: 'e@mail.com'
    , password: 'abc123'
    , timeout$: 5000
    , roles: ['admin']
    , name: {
        first: 'user'
      , last: 'one'
      }
    , meta$: {
        user: {superuser: true}
      }
    })

    tt.match(user, {
      username: 'userone'
    , email: 'e@mail.com'
    , roles: ['admin']
    , permissions: {
        auth: {
          user: {
            create: true
          }
        }
      }
    })
  })

  await t.test('duplicate emails not allowed', async (tt) => {
    try {
      const user = await hemera.act({
        topic: 'user'
      , cmd: 'create'
      , version: 'v1'
      , username: 'userone'
      , email: 'e@mail.com'
      , roles: ['admin']
      , name: {
          first: 'user'
        , last: 'one'
        }
      , meta$: {
          user: {superuser: true}
        }
      })
      tt.fail('test should throw')
    } catch (err) {
      tt.type(err, Error)
      tt.equal(err.name, 'DuplicatePrimaryKeyError')
      tt.pass('duplicate emails not allowed')
    }
  })

  await t.test('cannot assing invalid role', async (tt) => {
    try {
      const user = await hemera.act({
        topic: 'user'
      , cmd: 'create'
      , version: 'v1'
      , username: 'usertwo'
      , email: 'f@mail.com'
      , roles: ['fake']
      , name: {
          first: 'user'
        , last: 'one'
        }
      , meta$: {
          user: {superuser: true}
        }
      })
      tt.fail('test should throw')
    } catch (err) {
      tt.type(err, Error)
      tt.equal(err.name, 'InvalidRoleError')
      tt.pass('invalid roles not allowed')
    }
  })

  await t.test('user create permission required', async (tt) => {
    try {
      const user = await hemera.act({
        topic: 'user'
      , cmd: 'create'
      , version: 'v1'
      , username: 'usertwo'
      , email: 'f@mail.com'
      , roles: ['fake']
      , name: {
          first: 'user'
        , last: 'one'
        }
      , meta$: {
          user: {
            permissions: {
              auth: {
                user: {
                  create: false
                }
              }
            }
          }
        }
      })
      tt.fail('test should throw')
    } catch (err) {
      tt.type(err, Error)
      tt.equal(err.code, 'EAUTH')
      tt.pass('create permission required')
    }
  })

}).catch(threw)

test('teardwon', async(t) => {
  await teardown()
  t.pass('teardown complete')
})
