'use strict'
const util = require('util')
const conf = require('keef')
const hemera = require('./lib/hemera')
const services = require('./lib/fs/loading/service')


if (require.main === module) {
  hemera.ready(() => {
    hemera.log.info('auth service started')
    services.load()
  })
}

module.exports = hemera

