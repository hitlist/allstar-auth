'use strict'

const validate = require('uuid-validate')
const hemera = require('../../lib/hemera')
const {HemeraError} = require('nats-hemera').errors
const {connection} = require('zim/lib/db')
const {r, Errors} = connection.default
const User = require('../models/user')
const joi = hemera.joi

module.exports =
hemera.add({
  topic: 'user'
, cmd: 'setpassword'
, version: 'v1'
, auth: {
    permissions: 'auth:user:modify'
  }
, email: joi.string().required().description('user email address or exact id')
, password: joi.string().required().description('the new password to set')
}, async function setPassword({email, password}) {
  let user_id = validate.version(email) ? email : r.uuid(email)
  try {
    this.log.info(`setting new password for user ${email}`)
    const user = await User.get(user_id)
    await user.setPassword(password)
    await user.save()
    return user
  } catch (e) {
    if (e instanceof Errors.DocumentNotFound) {
      this.log.error(e.msg || e.message)
      const error = new HemeraError({
        message: 'Not Found'
      , status: 404
      , code: 'ENOENT'
      , name: 'EntityNotFoundError'
      })
      throw error
    }
    throw e
  }
})
