'use strict'

const validate = require('uuid-validate')
const hemera = require('../../lib/hemera')
const {HemeraError} = require('nats-hemera').errors
const {connection} = require('zim/lib/db')
const {r, Errors} = connection.default
const User = require('../models/user')
const TABLE = User.getTableName()
const PK = User._pk

module.exports =
hemera.add({
  topic: 'user'
, cmd: 'mget'
, version: 'v1'
}, async function(req) {
  const keys = req.keys
    ? Array.isArray(req.keys)
      ? req.keys
      : [req.keys]
    : []

  this.log.debug('mget: looking up users', keys)
  this.delegate$.op = keys
  const lookups = keys.map((key) => {
    return validate.version(key) ? key : r.uuid(key)
  })
  const users = await User.getAll(...lookups, {index: User.pk()}).filter({active: true})
  return r.object(
    'meta'
  , r.object(
      'total'
    , users.length
    , 'limit'
    , null
    , 'offset'
    , null
    , 'next'
    , null
    , 'previous'
    , null
    )
  , 'data'
  , users
  )
})
