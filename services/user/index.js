"use strict"

module.exports = {
  create: require('./create')
, list: require('./list')
, get: require('./get')
, mget: require('./mget')
, login: require('./login')
, setPassword: require('./set-password')
}
