'use strict'

const herema = require('../../lib/hemera')
const {HemeraError} = require('nats-hemera').errors
const User = require('../models/user')

module.exports =
herema.add({
  topic: 'user'
, cmd: 'login'
, version: 'v1'
}, async function login ({email, password}) {
  let users
  this.log.debug('logging user in', email)
  try {
    users = await User.filter({
      active: true
    , email: email
    }).limit(1)
  } catch (e) {
    this.log.error(e)
    const error = new HemeraError({
      message: 'Not Authorized'
    , status: 401
    , code: 'EAUTH'
    , name: 'EntityAuthenticationError'
    })
    throw error
  }

  if (!users.length) {
    const error = new HemeraError({
      message: 'Not Authorized'
    , status: 401
    , code: 'EAUTH'
    , name: 'EntityAuthenticationError'
    })
    throw error
  }

  const user = users[0]

  if (!user || user && user.archived) {
    const error = new HemeraError({
      message: 'Not Authorized'
    , status: 401
    , code: 'EAUTH'
    , name: 'EntityAuthenticationError'
    })
    throw error
  }

  const valid = await user.checkPassword(password)
  if (!valid) {
    const error = new HemeraError({
      message: 'Not Authorized'
    , status: 401
    , code: 'EAUTH'
    , name: 'EntityAuthenticationError'
    })
    throw error
  }

  this.log.debug('log in successful', email)
  const permissions = await user.getAllPermissions()
  return {
    id: user.pk()
  , email: user.email
  , username: user.username
  , name: user.name
  , roles: user.roles
  , superuser: user.superuser
  , permissions: permissions
  }
})
