'use strict'

const validate = require('uuid-validate')
const hemera = require('../../lib/hemera')
const {HemeraError} = require('nats-hemera').errors
const {connection} = require('zim/lib/db')
const {r, Errors} = connection.default
const User = require('../models/user')
const DB = connection.default._config.db
const TABLE = User.getTableName()
const PK = User._pk

module.exports =
hemera.add({
  topic: 'user'
, cmd: 'get'
, version: 'v1'
, auth$: {
    permissions: 'auth:user:read'
  }
}, async function(req) {
  const user_id = req.user_id
  const index = validate.version(user_id) ? 'allstar_auth_user_id' : 'email'
  try {
    this.log.info('looking up user', user_id, index)
    const res = await User.getAll(user_id, {index}).filter({
      active: true
    }).secure()

    return res.length ? res[0] : null
  } catch (e) {
    if (e instanceof Errors.DocumentNotFound) {
      this.log.error(e.msg || e.message)
      const error = new HemeraError({
        message: 'Not Found'
      , status: 404
      , code: 'ENOENT'
      , name: 'EntityNotFoundError'
      })
      throw error
    }
    throw e
  }
})
