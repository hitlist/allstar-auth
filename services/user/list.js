'use strict'

const {connection} = require('zim/lib/db')
const {filter} = require('@allstar/reql-builder')
const sort = require('@allstar/reql-builder/lib/sort')
const hemera = require('../../lib/hemera')
const User = require('../models/user')
const r = connection.default.r
const table = User.getTableName()

module.exports =
hemera.add({
  topic: 'user'
, cmd: 'list'
, version: 'v1'
, auth$: {
    permissions: 'auth:user:read'
  }
}, async function(req) {
  const {limit = 25, offset= 0, filters = {}} = req
  const opts = filter(r, filters)

  return await r.object(
    'meta'
  , r.object(
      'total'
    , r.table(table).filter({active: true}).filter(opts).count()
    , 'limit'
    , limit
    , 'offset'
    , offset
    , 'next'
    , null
    , 'previous'
    , null
    )
  , 'data'
  , r.table(table)
    .orderBy({
      index: r.asc('user_last_name')
    })
    .without('password')
    .filter({active: true})
    .filter(opts)
    .skip(offset)
    .limit(limit)
    .coerceTo('array')
  )
})
