'use strict'

const connection = require('zim/lib/db').connection
const User = require('../models/user')
const Role = require('../models/role')
const hemera = require('../../lib/hemera')
const {bcrypt} = require('../../lib/token/hasher')
const r = connection.default.r
const DB_NAME = connection.default._config.db

module.exports =
hemera.add({
  topic: 'user'
, cmd: 'create'
, version: 'v1'
, auth$: {
    permissions: 'auth:user:create'
  }
}, async function createUser(req) {
  const opts = req
  opts.roles = opts.roles || []
  opts.roles = !Array.isArray(opts.roles)
    ? [opts.roles]
    : opts.roles

  await _validateRoles(opts.roles)
  if (opts.password != null) {
    this.log.debug('generating initial password for user', opts.email)
    const salt = await bcrypt.salt()
    opts.password = await bcrypt.encode(opts.password, salt)
  }
  opts.superuser = undefined
  opts.permissions = undefined
  const u = await new User(opts).save()
  const permissions = await u.getAllPermissions()
  this.log.info('new user created', u.email, u.pk)

  return Object.assign({}, u, {
    password: undefined
  , permissions: permissions
  })
})

function _validateRoles(roles) {
  if (!Array.isArray(roles) || !roles.length) return Promise.resolve(true)
  return new Promise((resolve, reject) => {
    r
      .db(DB_NAME)
      .table(Role.getTableName())
      .pluck('name')('name')
      .catch(reject)
      .then((_roles) => {
        const role_set = new Set(_roles)
        for (const role of roles) {
          if (!role_set.has(role)) {
            const error = new Error(`invalid role - ${role}:: ${_roles.join()}`)
            error.name = 'InvalidRoleError'
            error.code = 'EROLE'
            return reject(error)
          }
        }
        resolve(true)
      })
  })
}
