'use strict'

const {connection} = require('zim/lib/db')
const {filter, toResponse} = require('@allstar/reql-builder')
const sort = require('@allstar/reql-builder/lib/sort')
const hemera = require('../../lib/hemera')
const Role = require('../models/role')
const r = connection.default.r
const table = Role.getTableName()

module.exports =
hemera.add({
  topic: 'role'
, cmd: 'list'
, version: 'v1'
, auth$: {
    permissions: 'auth:role:read'
  }
}, async function(req) {
  const {limit = 25, offset= 0, filters = {}} = req
  return await toResponse(r, table, {
    filters
  , orderby: '-name'
  , orderby_index: 'name'
  , limit
  , offset
  })
})
