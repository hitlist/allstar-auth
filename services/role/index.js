'use strict'
/**
 * Operations for dealing with role based permissins
 * @module allstar-auth/services/role
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires allstar-auth/services/role/create
 * @requires allstar-auth/services/role/list
 */

module.exports = {
  create: require('./create')
, list: require('./list')
, get: require('./get')
}
