'use strit'

const validate = require('uuid-validate')
const hemera = require('../../lib/hemera')
const {HemeraError} = require('nats-hemera').errors
const {connection} = require('zim/lib/db')
const Role = require('../models/role')
const {r, Errors} = connection.default

module.exports =
hemera.add({
  topic: 'role'
, cmd: 'get'
, version: 'v1'
, auth$: {
    permissions: 'auth:role:read'
  }


}, async function(req) {
  const role = validate.version(req.role) ? role : r.uuid(req.role)
  try {
    return await Role.get(role)
  } catch (e) {
    if (e instanceof Errors.DocumentNotFound) {
      this.log.error(e.msg || e.message)
      const error = new HemeraError({
        message: 'Not Found'
      , status: 404
      , code: 'ENOENT'
      , name: 'EntityNotFoundError'
      })
      throw error
    }
    throw e
  }
})
