'use strict'

/**
 * Default Group object containing a collection of permissions
 * @module allstar-auth/services/user/models/user
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires allstar-auth/services/models/role
 * @requires allsstar-auth/lib/hemera
 */

const Role = require('../models/role')
const hemera = require('../../lib/hemera')

module.exports =
hemera.add({
  topic: 'role'
, cmd: 'create'
, version: 'v1'
, auth$: {
    permissions: 'auth:role:create'
  }
}, async function createRole(req) {
  return new Role({
    name: req.name
  , permissions: req.permissions || {}
  }).save()
})
