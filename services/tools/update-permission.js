'use strict'

const validate = require('uuid-validate')
const {connection} = require('zim/lib/db')
const Role = require('../models/role')
const {r, Errors} = connection.default
const hemera = require('../../lib/hemera')

module.expors =
hemera.add({
  topic: 'tools'
, cmd: 'update-permissions'
, auth$: {
    superuser: true
  }
}, async function updatePermissions(req) {
  const role = validate.version(req.role) ? role : r.uuid(req.role)
  try {
    return await Role
      .get(r.uuid(req.role)).update(function(doc) {
        return {
          permissions: doc('permissions').merge(req.permissions)
        }
      })
  } catch(e) {
    if (e instanceof Errors.DocumentNotFound) {
      this.log.error(e.msg || e.message)
      const error = new HemeraError({
        message: 'Not Found'
      , status: 404
      , code: 'ENOENT'
      , name: 'EntityNotFoundError'
      })
      throw error
    }
    throw e
  }
})
