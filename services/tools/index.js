'use strict'

module.exports = {
  createsuperuser: require('./create-superuser')
, seeduser: require('./seed-user')
, updatepermission: require('./update-permission')
}
