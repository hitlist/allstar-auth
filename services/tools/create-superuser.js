'use strict'
const hemera = require('../../lib/hemera')

module.exports =
hemera.add({
  topic: 'tools'
, cmd: 'createsuperuser'
, auth$: {
    superuser: true
  }
}, async function createSuperuser(req) {
    const User = require('../models/user')
    const user = new User({
      username: req.username
    , email: req.email
    , superuser: true
    , name: req.name
    })

    if (req.password && req.password.trim()) {
      if (req.confirm !== req.password) {
        const error = new Error('Passwords do not match')
        error.code = 'EAUTH'
        throw error
      }
      await user.setPassword(req.password)
    }
    await user.save()
    return `user ${user.pk()} created`
})
