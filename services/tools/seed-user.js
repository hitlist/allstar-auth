'use strict'

/**
 * dev tool to seed user data
 * @module allstar-auth/services/tools/seed-user
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires os
 * @requires util
 * @requires path
 * @requires fs
 * @requires allstar-auth/services/models/user
 * @requires allsstar-auth/lib/hemera
 */

const os = require('os')
const util = require('util')
const path = require('path')
const fs = require('fs')
const User = require('../models/user')
const hemera = require('../../lib/hemera')

const readFile = util.promisify(fs.readFile)

let names = null

module.exports =
hemera.add({
  topic: 'tools'
, cmd: 'seed-user'
, auth$: {
    superuser: true
  }
}, async function seedUsers(req) {
  if (!names) names = await readFile(path.join(__dirname, 'fixture', 'words.txt'),'utf8')
  const list = names.split(os.EOL)

  const users = []
  for (let idx = 0; idx < 500; idx++) {
    const now = process.hrtime().reduce((acc, item) => {return acc + item}, 0)
    const first = _pick(list).replace(/\'s$/, '')
    const last = _pick(list).replace(/\'s$/, '')
    const username = `${first}_${last}_${now.toString(32)}`
    this.log.debug('adding user: ', username)
    users.push({
      username
    , email: `${username}@email.biz`
    , name: {
        first
      , last
      }
    })
  }
  return await User.save(users)
})

function _shuffle(list) {
  const min = _rand(3, list.length - 1)
  const max = _rand(3, list.length - 1)
  return list.sort((a, b) => {
    return min < max ? -1 : 1
  })
}

function _pick(arr) {
  return arr[ _rand(0, arr.length -1)]
}

function _rand (min, max) {
  return Math.round(min + (max - min) * Math.random())
}

