'use strict'

/**
 * Base user instance for dealing with authorization
 * @module allstar-auth/services/user/models/user
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires uuid
 * @requires zim/lib/db
 * @requires allstar-auth/services/user/lib/hasher/bcrypt
 */

const crypto = require('crypto')
const uuid = require('uuid')
const {connection} = require('zim/lib/db')
const Role = require('./role')
const hasher = require('../../lib/token/hasher').bcrypt
const fields = connection.default.type
const r = connection.default.r

const User = connection.default.createModel('allstar_auth_user', fields.object().schema({
  username: fields.string().required()
, created: fields.date().default(function(){
    return r.now()
  })
, name: fields.object().schema({
    first: fields.string().required()
  , last: fields.string().required()
  }).default(null).removeExtra()
, avatar_url: fields.string()
, email: fields.string().email().lowercase().required()
, permissions: fields.object().allowNull().default(function(){
    return {}
  })
, loginDate: fields.date().default(null)
, superuser: fields.boolean().default(false)
, active: fields.boolean().default(true)
, password: fields.string().default(null)
, roles: fields.array().schema(fields.string()).default(() => {
    return []
  })
, allstar_auth_user_id: fields.string().default(function(){
    return r.uuid(this.email.trim())
  })
}).removeExtra(), {
  pk: 'allstar_auth_user_id'
, table: {
    shards: 3
  , replicas: 2
  }
})

User.pre('save', function(next) {
  this.email = this.email.toLowerCase()
  if (!this.allstar_auth_user_id) {
    this.allstar_auth_user_id = r.uuid(this.email.trim())
  }

  if (!this.avatar_url) {
    const hash = crypto.createHash('md5').update(this.email).digest('hex')
    this.avatar_url = `https://www.gravatar.com/avatar/${hash}`
  }
  next()
})

User.defineStatic('pk', function() {
  return 'allstar_auth_user_id'
})

User.define('model', function() {
  return User
})

User.define('contentType', function(){
  return 'user'
})

User.define('pk', function() {
  return this.allstar_auth_user_id
})

/**
 * retuns user instances with out the password field
 * @method module:allstar-auth/services/user/models/user#secure
 **/
User.defineStatic('secure', function() {
  return this.without('password')
})

/**
 * returns user instances with all permissions merged into a single document
 * @method module:allstar-auth/services/user/models/user#withPermissions
 **/
User.defineStatic('withPermissions', function() {
  return this.merge(function( doc ){
    return r.object(
      'permissions',
      doc('roles').eqJoin(function( id ){
         return id
      }, r.table(Role.getTableName()), {index: 'name'})
      .default({}).zip()('permissions')
      .reduce(function(left, right ){
        return left.merge(right)
      }).default(doc('permissions'))
    )
  })
})
/**
 * Sets a new password on a user instance. The password with be salted and encoded before saved
 * @param {String} password the password to set
 * @param {String} [salt] A salt to use to create the encoded password with. One will be generated if not set
 * @chainable
 * @method module:allstar-auth/services/user/models/user#setPassword
 **/
User.define('setPassword', async function(password) {
  const salt = await hasher.salt()
  this.password = await hasher.encode(password, salt)
  return this
})

/**
 * Check a plain text password against the encoded password on the user instance
 * @method module:allstar-auth/services/user/models/user#checkPassword
 * @param {String} password the password to check
 * @return {Boolean}
 **/
User.define('checkPassword', function(txt) {
  if (!txt) return Promise.resolve(false)
  return hasher.verify(txt, this.password)
})

/**
 * Given a permissions string, resolves a true / false if a user has the given permission.
 * A permission string represensts an object heirarchy using a colon as a property accessor
 * @method module:allstar-auth/services/user/models/user# perms
 * @param {String} permission The permission to check
 * @return {Promise}
 * @example
 * user.hasPerms('auth') // has any permissions in the auth module
 * user.hasPerms('auth:user') // has any user specific permssions
 * user.hasPerms('auth:user:create') // can create a user instance
 **/
User.define('hasPermissions', function(permission = '') {
  return new Promise((resolve, reject) => {
    if (this.superuser) return resolve(true)
    this.getAllPermissions().then((p) => {
      resolve(!!get(p, permission, ':'))
    })
  })
})

/**
 * Resolves all permissions a user has
 * @method module:allstar-auth/services/user/models/user#getAllPermissions
 * @param {Promise}
 **/
User.define('getAllPermissions', function(){
  const user_id = this.id
  const _that = this
  if (this._perms) return Promise.resolve(this._perms)
  return new Promise((resolve, reject) => {
    if (_that.superuser) {
      Role
        .getField('permissions')
        .reduce((left, right) => {
          return left.merge(right)
        })
        .execute()
        .then((perms) => {
          Object.defineProperty(_that, '_perms', {
            enumerable: false
          , get: () => {
              return perms
            }
          })
          resolve(perms)
        })
        return
    }

    if( !Array.isArray(_that.roles) || !_that.roles.length ){
      return resolve( {} )
    }

    Role
      .getAll( r.args(_that.roles),{index:'name'} )
      .getField('permissions')
      .reduce(function( left, right ){
        return left.merge( right );
      })
      .execute()
      .then(function(perms){
        Object.defineProperty(_that,"_perms",{
          enumerable:false,
          get: function(){
            return perms;
          }
        });
        resolve( perms );
      });
  })
})

function get(obj, prop, sep = '.') {
  var parts = prop.split(sep),
  last = parts.pop();
  while (prop = parts.shift()) {
    obj = obj[prop];
    if (typeof obj !== 'object') return;
  }
  return obj[last];
}

User.defineStatic('indexes', function() {
  return Promise.all([
    User.ensureIndex('active')
  , User.ensureIndex('email')
  , User.ensureIndex('username')
  , User.ensureIndex('user_last_name', function(doc) {
      return doc('name')('last')
    })
  ])
})

User.ensureIndex('active')
User.ensureIndex('email')
User.ensureIndex('username')
User.ensureIndex('user_last_name', function(doc) {
  return doc('name')('last')
})
module.exports = User
