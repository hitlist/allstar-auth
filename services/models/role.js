'use strict'
/**
 * Default Group object containing a collection of permissions
 * @module allstar-auth/services/user/models/user
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires zim/lib/db
 */

const crypto     = require('crypto')
const connection = require('zim/lib/db').connection
const r          = connection.default.r
const fields     = connection.default.type


const Role = connection.default.createModel('allstar_auth_role', {
  name: fields.string().required()
, permissions: fields.object()
}, {
  pk:'auth_role_id'
, table: {
    shards: 3
  , replicas: 2
  }
});

Role.defineStatic('indexes', function() {
  return Promise.all([
    Role.ensureIndex('name')
  ])
})

Role.pre('save', function() {
  if (this.auth_role_id) return
  this.auth_role_id = r.uuid(this.name.toLowerCase())
})

Role.ensureIndex('name')
module.exports = Role
