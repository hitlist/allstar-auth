'use strict'

const cli = require('seeli')

module.exports = new cli.Command({
  description: 'create a new user with a superuser grant'
, alias: 'createsuperuser'
, usage: [
    `${cli.bold('Usage:')} allstar create superuser -u esatterwhite -F Eric -L Satterwhite`
  , `${cli.bold('Usage:')} allstar create superuser -u joeblow --name:first=Joe --name:last=Blow`
  ]
, flags: {
    username: {
      type: String
    , shorthand: 'u'
    , required: true
    , description: 'A unique username for the user'
    }
  , email: {
      type: String
    , shorthand: 'e'
    , requiredd: true
    , description: 'The email address for the user'
    }
  , 'name:first': {
      type: String
    , shorthand: 'F'
    , required: true
    , description: 'The user\'s first name'
    }
  , 'name:last': {
      type: String
    , shorthand: 'L'
    , required: true
    , description: 'The user\'s last name'
    }
  , password: {
      type: String
    , mask: true
    , shorthand: 'p'
    , required: false
    , description: 'Password'
    }
  , confirm: {
      type: String
    , mask: true
    , required: false
    , shorthand: 'pp'
    , description: 'Confirm password'
    }
  }
, run: async (cmd, data, done) => {
    const User = require('../services/models/user')
    const user = new User({
      username: data.username
    , email: data.email
    , superuser: true
    , name: data.name
    })

    if (data.password && data.password.trim()) {
      if (data.confirm !== data.password) {
        const error = new Error('Passwords do not match')
        error.code = 'EAUTH'
        done(error)
        return process.exit(1)
      }
      await user.setPassword(data.password)
    }

    try {
      await user.save()
      done(null, `user ${user.pk()} created`)
      process.exit(0)
    } catch (e) {
      done(e)
      process.exit(1)
    }
  }
})
