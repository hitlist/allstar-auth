# 0000 BASE
FROM mhart/alpine-node:8 as base

RUN apk update && apk upgrade && apk add python make g++

RUN mkdir -p /opt/app
WORKDIR /opt/app
ARG NPM_TOKEN

COPY package.json package-lock.json /opt/app/

RUN npm install --production

COPY . /opt/app

# RELEASE
FROM mhart/alpine-node:8 as release
RUN addgroup -g 1000 allstar \
  && adduser -u 1000 -G allstar -D -s /bin/sh -h allstar allstar

ENV logger=stdout
ENV log__stdout__level=debug
ENV DEBUG=*

COPY --from=base /usr/bin /usr/bin
COPY --from=base /usr/include/node /usr/include
COPY --from=base /usr/lib/node_modules/ /usr/lib/node_modules/
COPY --from=base --chown=allstar:allstar /opt/app/ /opt/app/

WORKDIR /opt/app
RUN npm link
USER allstar
CMD ["node", "index.js"]
