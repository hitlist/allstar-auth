#!/usr/bin/env node

'use strict'

const seeli = require('seeli')
const commands = require('../commands')

for (const [name, command] of commands) {
  seeli.use(name, command)
}

seeli.exitOnError = true
seeli.set('color', 'magenta')
seeli.run()
