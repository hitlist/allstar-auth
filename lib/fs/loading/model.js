'use strict'
/**
 * Loader to autodiscover models
 * @module allstar-auth/lib/fs/loading/user
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires path
 **/

const path = require('path')
const Loader = require('gaz/fs/loader')

class Models extends Loader {
  constructor(options) {
    super(options)
  }

  remap(app, loaded) {
    const name = path.parse(loaded.path)
      .name.replace(/^(\w){1}/, (match, p1) => {
        return p1.toUpperCase()
      })
    return {
      [name]: require(loaded.path)
    }
  }

  flat() {
    const acc = Object.create(null)
    for (const items of Object.values(this)) {
      items.reduce((a, item) => {
        return Object.assign(a, item)
      }, acc)
    }
    return acc
  }
}

module.exports = new Models({
  searchpath: 'services/models'
, filepattern: /\.js$/
, recurse: false
})
