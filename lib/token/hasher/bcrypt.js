'use strict'

/**
 * Base hasher impmlementation
 * @author Eric Satterwhite
 * @since 0.1.0
 * @module allstar-auth/services/token/hasher/bcrypt
 * @requires crypto
 * @requires bcryptjs
 */

const crypto = require('crypto')
const bcrypt = require('bcryptjs')
const SEP = '::'

/**
 * bcrypt base token generator
 * @constructor
 * @alias module:allstar-auth/services/token/hasher/bcrypt
 * @param {Object} [options]
 * @param{number} [options.inerations=6] the never of encryption cycles to apply to the token
 **/
module.exports = class BcryptHasher {
  constructor(options = {}) {
    this.options = Object.assign({
      iterations: 6
    }, options, {
      algorithm: 'bcrypt-sha256'
    })
  }

  /**
   *
   * @method module:allstar-auth/services/token/hasher/bcrypt#encode
   * @param {String} txt The text to encrypt
   * @param {String} salt a string valut to use to use as the encryption salt
   * @promise encrypted
   * @fulfill {String} The encrypted version of the passed in text
   * @reject {Error} returned when the encryption fails
   **/
  encode(txt, salt) {
    return new Promise((resolve, reject) => {
      const hasher = crypto.createHash('sha256')
      hasher.update(txt)
      bcrypt.hash(hasher.digest('hex'), salt, (err, data) => {
        if (err) return reject(err)
        resolve(`${this.options.algorithm}${SEP}${data}`)
      })
    })
  }

  /**
   * Ranomly generats a new salt value
   * @method module:allstar-auth/services/token/hasher/bcrypt#salt
   * @promise {SaltPromise}
   * @fulfill {String} salt The salt value
   * @reject {Error} err An ecryptions failure
   **/
  salt(){
    return new Promise((resolve, reject) => {
      bcrypt.genSalt(this.options.interations, (err, salt) => {
        if (err) return reject(err)
        resolve(salt)
      })
    })
  }

  /**
   * Verifies a plain text value against it's encoded counter part
   * @method module:allstar-auth/services/token/hasher/bcrypt#verify
   * @param {String} text Plain text string to verify
   * @param {String} encrypted The encrytped version of the plain text string to compare against
   * @promise ValidatedPromis
   * @fulfill {Boolean} true if the text value passes verification
   * @rejects InvalidAlgorithmError Returned when an invalid encryption type was found
   **/
  verify(txt, encoded){
    const bits = encoded.split(SEP)
    const algorithm = bits[0]
    return new Promise((resolve, reject) => {
      if (algorithm !== this.options.algorithm) {
        const error = new Error('Invalid algorithm type')
        error.name = 'InvalidAlgorithmError'
        error.code = 'ENOALGORITHM'
        return setImmediate(reject, error)
      }
      const hasher = crypto.createHash('sha256')
      const compare = hasher.update(txt).digest('hex')
      bcrypt.compare(compare, bits[1], (err, matched) => {
        if (err) return reject(err)
        resolve(matched)
      })
    })
  }
}

